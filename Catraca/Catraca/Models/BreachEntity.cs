﻿using catraca;
using Microsoft.WindowsAzure.Storage.Table;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Catraca.Models
{
    public class BreachEntity : TableEntity
    {
        public BreachEntity(DateTime dateTime)
        {
            DateTime partition = new DateTime(dateTime.Year, dateTime.Month, dateTime.Day);
            PartitionKey = partition.ToUniversalTime().Ticks+"";
            RowKey = dateTime.ToUniversalTime().Ticks+"";

        }

    }
}