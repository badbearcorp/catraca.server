﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PushSharp;
using PushSharp.Android;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data;


namespace catraca
{
    public class AndroidPushing
    {
        PushBroker push;

        public AndroidPushing(String apiKey)
        {
            push = new PushBroker();

            //Wire up the events for all the services that the broker registers
            push.OnNotificationSent += NotificationSent;
            push.OnChannelException += ChannelException;
            push.OnServiceException += ServiceException;
            push.OnNotificationFailed += NotificationFailed;
            push.OnDeviceSubscriptionExpired += DeviceSubscriptionExpired;
            push.OnDeviceSubscriptionChanged += DeviceSubscriptionChanged;
            push.OnChannelCreated += ChannelCreated;
            push.OnChannelDestroyed += ChannelDestroyed;


            push.RegisterGcmService(new GcmPushChannelSettings(apiKey));

        }

        private static void ChannelDestroyed(object sender)
        {
            string x = sender.ToString();
        }

        private static void ChannelCreated(object sender, PushSharp.Core.IPushChannel pushChannel)
        {
            string x = sender.ToString();
        }

        private static void DeviceSubscriptionChanged(object sender, string oldSubscriptionId, string newSubscriptionId, PushSharp.Core.INotification notification)
        {
            string x = sender.ToString();
        }

        private static void DeviceSubscriptionExpired(object sender, string expiredSubscriptionId, DateTime expirationDateUtc, PushSharp.Core.INotification notification)
        {
            string x = sender.ToString();
        }

        private static void NotificationFailed(object sender, PushSharp.Core.INotification notification, Exception error)
        {
            string x = sender.ToString();
        }

        private static void ServiceException(object sender, Exception error)
        {
            string x = sender.ToString();
        }

        private static void ChannelException(object sender, PushSharp.Core.IPushChannel pushChannel, Exception error)
        {
            string x = sender.ToString();
        }

        private static void NotificationSent(object sender, PushSharp.Core.INotification notification)
        {
            string x = sender.ToString();
        }


        public void Queue(JObject mensagem, List<string> userRegId)
        {
            push.QueueNotification(new GcmNotification().ForDeviceRegistrationId(userRegId)
                .WithJson(
                    (string)JsonConvert.SerializeObject(
                        mensagem, new JsonSerializerSettings() { Formatting = Newtonsoft.Json.Formatting.None })
            ));
        }

        public void Fire()
        {
            push.StopAllServices();
        }
    }
}