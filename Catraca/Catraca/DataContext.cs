﻿using Catraca.Models;
using Microsoft.WindowsAzure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Catraca
{
    public class DataContext
    {
        private DataContext() { }
        private static DataContext _instance;
        public static DataContext Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new DataContext();
                return _instance;
            }
        }

        private CloudStorageAccount _storageAccount;

        private CloudStorageAccount StorageAccount
        {
            get
            {
                if(_storageAccount == null)
                    _storageAccount = CloudStorageAccount.Parse(
                        CloudConfigurationManager.GetSetting("StorageConnectionString")
                    );
                return _storageAccount;
            }
        }

        private CloudTable BreachTable
        {
            get
            {
                return GetTable("breach");
            }
        }

        private CloudTable RegisterTable
        {
            get
            {
                return GetTable("register");
            }
        }

        private CloudTable GetTable(string name)
        {
                CloudTableClient tableClient = StorageAccount.CreateCloudTableClient();
                CloudTable table = tableClient.GetTableReference(name);
                table.CreateIfNotExists();
                return table;
        }

        private CloudBlobContainer PhotosContainer
        {
            get
            {
                CloudBlobClient blobClient = StorageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobClient.GetContainerReference("photos");
                container.CreateIfNotExists();
                return container;
            }
        }

        public void Insert(BreachEntity entity,string photo)
        {
            TableOperation insert = TableOperation.Insert(entity);
            BreachTable.Execute(insert);

            CloudBlockBlob blob = PhotosContainer.GetBlockBlobReference(entity.RowKey);
            if (photo.StartsWith("\""))
                photo = photo.Remove(0, 1);
            if (photo.EndsWith("\""))
                photo = photo.Remove(photo.Length - 2, 1);
            blob.UploadText(photo);
        }

        public string GetPhoto(string id)
        {
            CloudBlockBlob blob = PhotosContainer.GetBlockBlobReference(id);
            string photo = blob.DownloadText();
            
            if (photo.StartsWith("\""))
                photo = photo.Remove(0, 1);
            if (photo.EndsWith("\""))
                photo = photo.Remove(photo.Length - 2, 1);

            return photo;
        }



        public void Insert(RegisterEntity entity)
        {
            TableOperation insert = TableOperation.Insert(entity);
            RegisterTable.Execute(insert);
        }


        public List<string> RegIds {
            get
            {
                List<string> retVal = new List<string>();
                TableQuery<RegisterEntity> query = new TableQuery<RegisterEntity>();
                var ids = RegisterTable.ExecuteQuery(query).ToList();
                foreach(RegisterEntity e in ids)
                {
                    retVal.Add(e.RowKey);
                }
                return retVal;
            }
        
        }

        public string[] PhotoIds
        {
            get
            {
                List<String> retVal = new List<String>();
                foreach (IListBlobItem item in PhotosContainer.ListBlobs(null, false))
                {
                    retVal.Add(item.Uri.Segments[item.Uri.Segments.Length-1]);
                }
                return retVal.ToArray();
            }
        }
    }
}