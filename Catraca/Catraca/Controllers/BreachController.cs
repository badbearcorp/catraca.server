﻿using catraca;
using Catraca.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Catraca.Controllers
{
    public class BreachController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [HttpGet]
        public HttpResponseMessage PhotoIds()
        {
            string[] ids = DataContext.Instance.PhotoIds;
            JArray arr = new JArray(ids);

            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new StringContent(arr.ToString());
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return result;
        }

        [HttpGet]
        public HttpResponseMessage Photo(string id)
        {
            string photo = DataContext.Instance.GetPhoto(id);
            byte[] b = Convert.FromBase64String(photo);

            var result = new HttpResponseMessage(HttpStatusCode.OK);
            result.Content = new ByteArrayContent(b);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");

            return result;
        }

        // POST api/breach
        public void Post([FromBody]string data)
        {
            BreachEntity entity = new BreachEntity(DateTime.Now);
            DataContext.Instance.Insert(entity,data);
            JObject obj = new JObject();
            obj["RowKey"] = entity.RowKey;

            AndroidPushing pushing = new AndroidPushing("AIzaSyCX5JIUdeUM5WKIpPP4U6QAmY2SGs5hmKs");
            List<string> regids = DataContext.Instance.RegIds;
            pushing.Queue(obj, regids);
            pushing.Fire();

        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
