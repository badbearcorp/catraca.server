﻿using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Catraca.Models
{
    public class RegisterEntity : TableEntity
    {
        public RegisterEntity() { }
        public RegisterEntity(string gcm, string device)
        {
            PartitionKey = device;
            RowKey = gcm;
        }


    }
}