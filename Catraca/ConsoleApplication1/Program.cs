﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            byte[] b = File.ReadAllBytes(args[0]);
            string base64String = Convert.ToBase64String(b);
            base64String = "\"" + base64String + "\"";
            //WebRequest request = WebRequest.Create("http://catraca.azurewebsites.net/api/Breach");
            WebRequest request = WebRequest.Create("http://localhost:49122/api/Breach");
        
            request.Method = "POST";
            request.ContentLength = base64String.Length;
            request.ContentType = "application/json";
            Stream s = request.GetRequestStream();
            b = System.Text.Encoding.UTF8.GetBytes(base64String);
            s.Write(b,0,b.Length);
            s.Close();
            Console.WriteLine(((HttpWebResponse)request.GetResponse()).StatusCode);
            Console.ReadLine();
        }

    }
}
