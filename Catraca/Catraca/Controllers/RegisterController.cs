﻿using Catraca.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Catraca.Controllers
{
    public class RegisterController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }


        public IHttpActionResult PostRegister(JObject jsonData)
        {
            dynamic json = jsonData;
            string gcm = json.gcm;
            string device = json.device;

            RegisterEntity entity = new RegisterEntity(gcm, device);
            DataContext.Instance.Insert(entity);

            return Ok();
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
